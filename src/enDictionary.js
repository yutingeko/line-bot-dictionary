const superagent = require('superagent');
const cheerio = require('cheerio');

function searchEng(keyword) {
  return new Promise((resolve, reject)=> {
    let reptileUrl = `https://dictionary.cambridge.org/dictionary/english-chinese-traditional/${keyword}`;
    let data = [];
    superagent.get(reptileUrl).end(function (err, res) {
      if (err) {
        reject(err);
      }
      let $ = cheerio.load(res.text);
      $('.def-block').each(function (i, elem) {
        if(data.length > 0) return;
        data.push({
          engDef: $(elem).find('.def').text().trim(),
          cnDef: $(elem).find('.def-body > .trans').text().trim()
        });
        data[0].voiceUS = $('.us').find('[data-src-mp3]').data('src-mp3') || $('.uk').find('[data-src-mp3]').data('src-mp3');
      });
      data.length > 0 ? resolve(data): reject();
    });
  })
}

module.exports = searchEng;