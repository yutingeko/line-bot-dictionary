let searchEng = require('./enDictionary');
let { getSheetsData, writeSheetsData } = require('./googleSheets');
let storedData = Object.create(null);

const USAGE = `使用說明：\n
1. 本週/本週pdf：本週討論討論主題\n
2. 歷史/歷史pdf：以前討論過的主題\n
3. [Vocabulary]: 直接輸入英文單字即可查單字\n
4. 單字: 查看已儲存的單字\n
5. 小黃掰： 關閉功能`;

function confirmTemplate(data, altText) {
  return {
    altText,
    type: 'confirm',
    actions: [{
      type: 'postback',
      label: '儲存',
      data: JSON.stringify(data),
    },{
      type: 'message',
      label: '否',
      text: '否'
    }]
  }
}

function buttonTemplate(data, altText) {
  let voice = data.vocabulary.voiceUS;
  delete data.vocabulary.voiceUS
  return {
    altText,
    type: 'buttons',
    actions: [{
      type: 'postback',
      label: '儲存',
      data: JSON.stringify(data),
    }, {
      type: 'uri',
      label: '發音',
      uri: voice
    }]
  }
}

function showVoc(id) {
  return new Promise((resolve, reject)=> {
    getSheetsData('recordVocabularies').then(data=> {
      let filterIdData = data.filter(item=> item.indexOf(id) !== -1)
      storedData[id].vocList = filterIdData;
      resolve(filterIdData.map(item=> `${item[1]}  ${item[3]}`).join('\n'))
    }).catch(()=> reject('嗯...好像哪裡出錯了'));
  })
}

function showInfo(id, msg) {
  let pdfFlag = msg.match('pdf');
  return new Promise((resolve, reject)=> {
    getSheetsData('weeklyTopic').then(data=> {
      let filterIdData = data.filter(item=> item.indexOf(id) !== -1);
      let finalData = msg.match('本週') ? filterIdData.filter((item,idx)=> idx === filterIdData.length-1) :
        filterIdData.filter((item,idx)=> idx !== filterIdData.length-1);
      storedData[id].allTopic = filterIdData
      resolve(finalData.map(item=> pdfFlag ? item[3]: item[2]).join('\n'));
    }).catch(()=> reject('嗯...好像哪裡出錯了'));
  })
}

function lookUpTheWord(id, search) {
  let searchStr = search.toLowerCase();
  return new Promise((resolve, reject)=> {
    searchEng(searchStr).then( ([res]) => {
      res.vocabulary = searchStr;
      res.engDef = res.engDef.length < 150 ? res.engDef : '';
      resolve(res);
    }).catch(()=>{
      reject('抱歉我找不到這個字(ಥ﹏ಥ)');
    })
  })
}

function helpAction(msg, dataFlag, id, name) {
  let dataAction = (action='init')=> {
    action === 'init' ? storedData[id] = { name } : delete storedData[id]
  }
  switch(msg) {
    case '小黃':
      if(!dataFlag) {
        dataAction('init')
        return `嗨, 我是小黃，很高興能為你服務\n\n${ USAGE }`
      } else {
        return `${ USAGE }`
      }
    case '小黃掰':
      dataAction('delete');
      return '掰掰～'
  }
}

async function msgProcess(event) {
  let msg = event.message.text;
  let id = event.source.groupId || event.source.roomId || event.source.userId;
  let dataFlag = Object.prototype.hasOwnProperty.call(storedData, id);
  let profile = await event.source.profile();

  if(msg.match('小黃')) {
    return {sendMsg: helpAction(msg, dataFlag, id, profile.displayName)}
  } else if(dataFlag) {
    let search = msg.match(/[a-zA-Z]+/) && msg.match(/[a-zA-Z]+/)[0] || null;
    let topicUrl = msg.match('breakingnewsenglish.com') && msg.match(/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/)[0] || null;
    switch(msg){
      case '本週':
      case '本週pdf':
      case '歷史':
      case '歷史pdf':
        return {sendMsg: await showInfo(id, msg)};
      case '單字':
        return {sendMsg: await showVoc(event.source.userId)}
      default:
        if(topicUrl) {
          return {sendMsg: '想紀錄下週主題嗎？', action: confirmTemplate({topicUrl},'想紀錄下週主題嗎？')};
        } else if(search) {
          let res = await lookUpTheWord(id, search);
          return typeof res === "object" ? {
            sendMsg: `${search}\n${res.engDef }\n${res.cnDef}`,
            action: buttonTemplate({vocabulary: res}, search)
          } : { sendMsg: res }
        }
    }
  }
}

async function postbackProcess(event) {
  let {topicUrl= null, vocabulary= null} = JSON.parse(event.postback.data);
  let id = event.source.groupId || event.source.roomId || event.source.userId;
  let timestamp = event.timestamp;

  if(topicUrl) {
    let topicUrlPdf = topicUrl &&  topicUrl.match('-m.html') ? topicUrl.replace('.html','.pdf') : topicUrl.replace('.html','-m.pdf');
    if(!storedData[id].allTopic) {
      await getSheetsData('weeklyTopic').then(data=> {
        storedData[id].allTopic = data.filter(item=> item.indexOf(id) !== -1)
      }).catch(()=> reject('嗯...好像哪裡出錯了'));
    }
    if(storedData[id].allTopic.filter(item=> item.indexOf(topicUrl) !== -1).length > 0) {
      return Promise.reject('已儲存此篇')
    } else {
      return await writeSheetsData('weeklyTopic', [id,timestamp,topicUrl,topicUrlPdf]);
    }
  }

  if(vocabulary) {
    await getSheetsData('recordVocabularies').then(data=> {
      storedData[id].vocList = data.filter(item=> item.indexOf(id) !== -1)
    }).catch(()=> reject('嗯...好像哪裡出錯了'));
    if(storedData[id].vocList.filter(item=> item.indexOf(vocabulary.vocabulary) !== -1).length > 0) {
      return Promise.reject('單字已儲存')
    } else {
      return await writeSheetsData('recordVocabularies', [event.source.userId,vocabulary.vocabulary,vocabulary.engDef,vocabulary.cnDef])
    }
  }
}

module.exports = {msgProcess, postbackProcess};