const google = require('googleapis');
const {OAuth2Client} = require('google-auth-library');
const { googleAuthData } = require('../data/initData');
const { clientSecret, credentials } = googleAuthData;

//client_secret.json
const myClientSecret = clientSecret;
// var auth = new googleAuth();
const oauth2Client = new OAuth2Client(myClientSecret.installed.client_id,myClientSecret.installed.client_secret, myClientSecret.installed.redirect_uris[0]);

//sheetsapi.json
oauth2Client.credentials = credentials;

//試算表的id
const mySheetId='1X8vMZxswYflyxndjt_5e7dm5p8AaAcfN86oo4wnUTn4';

let users=[];
let myQuestions=[];

//get data
function getSheetsData(rangeTitle) {
  let sheets = google.sheets('v4');
  return new Promise((resolve, reject)=> {
    sheets.spreadsheets.values.get({
      auth: oauth2Client,
      spreadsheetId: mySheetId,
      range: rangeTitle,
      majorDimension: "ROWS",
    }, (err, response)=> {
      if (err) {
         reject('讀取問題檔的API產生問題：' + err);
      }
      resolve(response.data && response.data.values);
    });
  })
}

//store data
function writeSheetsData(rangeData, data) {
  let request = {
    auth: oauth2Client,
    spreadsheetId: mySheetId,
    range: rangeData,
    insertDataOption: 'INSERT_ROWS',
    valueInputOption: 'RAW',
    resource: {
      values: [data]
    }
  };
  return new Promise((resolve, reject)=> {
    google.sheets('v4').spreadsheets.values.append(request, (err, response)=> {
      !err ? resolve() : reject();
    });
  })
}


module.exports =  { getSheetsData, writeSheetsData}