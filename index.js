let linebot = require('linebot');
let express = require('express');
let { msgProcess, postbackProcess } = require('./src/actionProcess');
let { dataLineBot } = require('./data/initData');

//init linebot (channelId, channelSecret, channelAccessToken)
let bot = linebot(dataLineBot);

//process message
bot.on('message', event=> {
  msgProcess(event)
    .then(({sendMsg, action={}})=> sendMessage(event, sendMsg, action))
    .catch(errorMsg=> sendMessage(event, errorMsg));
});

bot.on('postback', event=> {
  postbackProcess(event)
    .then(()=> sendMessage(event, '儲存完畢'))
    .catch(err => sendMessage(event, err || '儲存失敗'))
});

//switch reply content
function switchType(sendMsg, action={}) {
  return action.type ? {
    type: 'template',
    altText: action.altText,
    template: {
      type: action.type,
      text: sendMsg,
      actions: action.actions
    }
  } : sendMsg;
}

//send message
function sendMessage(event, sendMsg, action={}){
  event.reply(switchType(sendMsg, action))
    .then(data=> data)
    .catch(error=> console.log(error));
}

let app = express();
let linebotParser = bot.parser();
app.post('/', linebotParser);

//因為 express 預設走 port 3000，而 heroku 上預設卻不是，要透過下列程式轉換
let server = app.listen(process.env.PORT || 8080, function() {
  let port = server.address().port;
  console.log("App now running on port", port);
});
